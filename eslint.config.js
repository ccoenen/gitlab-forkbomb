import js from "@eslint/js";
import globals from "globals";

export default [
	js.configs.recommended,
	{
		languageOptions: {
			ecmaVersion: 2022,
			sourceType: 'module',
			globals: { ...globals.node },
		},
		'rules': {
			'indent': [
				'error',
				'tab'
			],
			'linebreak-style': [
				'error',
			],
			'no-console': [
				'off'
			],
			'no-var': [
				'error',
			],
			'quotes': [
				'error',
				'single'
			],
			'semi': [
				'error',
				'always'
			]
		}
	}
];
