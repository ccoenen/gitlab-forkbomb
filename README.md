# Gitlab Forkbomb

Creating a bunch of users and then creating a bunch of forks from a given repository. This is supposed to facilitate creating a classroom-environment on GitLab. Essentially, a teacher creates a repository, and then creates a number of forks automatically, inviting each individual student to that fork.


## Setup

- Node.JS
- `npm install`
- update `config.js` with your admin credentials (copy from `config.js.example`)

You will need an access token, this can be set up in your admin-user's profile, the URL will look like `<gitlab-server>/-/profile/personal_access_tokens`. We need admin privileges, because we're [creating users](https://docs.gitlab.com/ee/api/users.html#user-creation).


## Usage

### Creating Users

~~Users get created automatically when necessary.~~ this is no longer the case! Users need to be existing and be retrievable uniquely by their name. Long-Term this should be switched over to the `invitation` API (see below, TODO).


### Creating Forks

`node src/fork.js <web-url of upstream repo to fork> <list of users>`

This command is idempotent, meaning that you can run it multiple times, and it should not be a problem on the first run, users and repositories are created, but on any subsequent run, existing users and projects are just updated.


### Removing Forks (careful!)

This will list all forks that could be removed: `node src/remove-forks.js <web-url of upstream repo that you forked from>`

If you really want to remove all these forks, supply another parameter to this command. The parameter is `--actually-delete-them-all`. ⚠️ Be extra careful here. Forks are deleted without further warning, and including any changes that were made. These will just be lost.


### Mass Git Actions

Generally: `node src/mass-git.js <web-url of upstream repo> <action> ...`
Destination will be the organisation path + repo path of each repository. (if you need to change this, pull requests are welcome!)
All of the actions need access to Gitlab, because we retrieve the list of forks from there.


#### Clone/Pull

`node src/mass-git.js <web-url of upstream repo> pullOrClone`

Will pull and/or clone the most current state.


#### Push

`node src/mass-git.js <web-url of upstream repo> push`

Will push all repos.


#### Tagging

`node src/mass-git.js <web-url of upstream repo> tag <someTagName>`

Will add a tag `<someTagName>` to the current commit and also pushes it to the server. Tag name defaults to `mass-git-YYYY-MM-DD` in case it's not provided.


#### Distribution of new Commits

In case your upstream changes, you might want to distribute those changes to all forks.

`node src/mass-git.js <web-url of upstream repo> distributeCommits`

This will fetch from the upstream repo, merge into the current branch. It will _not_ push back, use `push` action for that afterwards!


#### Resetting to some former state

In case you got yourself into an odd situation, you might want to reset the repository state; this will reset _hard_ to the state of origin.

`node src/mass-git.js <web-url of upstream repo> resetToOriginHard`


#### Checking for commits since some special timestamp

`node src/mass-git.js <web-url of upstream repo> commitsSince <timestamp>`

for example

`node src/mass-git.js <web-url of upstream repo> commitsSince "2021-12-16 01:15:00+01:00"`

shows a short summary what has changed on a repo since that date, across all branches.


#### Report Commits and Issues per Repository

`node src/report.js <web-url of upstream repo> text`

`node src/report.js <web-url of upstream repo> html > report.html`


#### Report Commits Directory-by-Directory per Repository

`node src/subpath-analysis.js <web-url of upstream repo>`

will output a CSV report.


### Archive

An archive run that git resets, git cleans and zips it up repo-by-repo

`node src/archive.js <web-url of upstream repo>`

### Userlist Format

Take a look at [`input/demo.csv`](input/demo.csv), the list of users is a simple csv file, utf-8 encoded. It should have a header (`name,email`) and then a list of users (of course their name and email address).


## Further Notes

In any place where we take a URL, you could also just use the `namespace/path` pair of a project. But since it's easier to just copy a URL, I suggest doing that.


# To be done

- is expiry a good idea for the ProjectMembers?
- usually, gitlab protects the main branch from developer-level members. This can be turned off in general settings, but we could also handle it differently (perhaps with another branch or merge-request?)
- Creating an issue for each one to contribute to
- as soon as Invitations land in gitbeaker, we could send out invitations to people instead of adding them as project member, this is tracked in https://github.com/jdalrymple/gitbeaker/issues/2484 / https://github.com/jdalrymple/gitbeaker/pull/2258

# Third Party Documentation

- https://docs.gitlab.com/ee/api/users.html#user-creation (api docs for creating a `User`)
- https://docs.gitlab.com/ee/api/projects.html#fork-project (api docs regarding `Projects`)
- https://docs.gitlab.com/ee/api/projects.html#list-user-projects (list user projects)
- https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project
- https://stackoverflow.com/a/43523014/2413043 (curl/gitlab fork example)
- https://github.com/jdalrymple/gitbeaker (library we use)
  - examples: https://github.com/jdalrymple/gitbeaker#examples
  - how sudo works: https://github.com/jdalrymple/gitbeaker#sudo
- SimpleGit: https://github.com/steveukx/git-js, also https://blog.jskoneczny.pl/post/run-git-commands-from-node-js-application-using-javascript
