import { dirname, basename } from 'node:path';

import chalk from 'chalk';
import {simpleGit, CleanOptions, ResetMode } from 'simple-git';
import { zip } from 'zip-a-folder';

import { forkIterator } from './fork-iterator.js';

const projectURL = process.argv[2];

forkIterator(projectURL, async (localPath, fork) => {
	//// good place to skip one in case of individual error
	// if (fork.name === 'something special') { continue; }
	console.log(`- processing ${fork.name}`);

	const git = simpleGit({baseDir: localPath});
	await git.reset(ResetMode.HARD);
	await git.clean(CleanOptions.FORCE);
	await git.clean(CleanOptions.FORCE, {'-X': null, '-d': null}); // cleans ignored files and untracked directories as well

	const parentDirectory = dirname(localPath);
	const outputFilename = parentDirectory + `/${basename(localPath)}-${basename(parentDirectory)}.zip`;
	console.log(`  - archiving ${localPath} at ${outputFilename}`);

	await zip(localPath, outputFilename);
});
