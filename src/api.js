import { createHash } from 'crypto';

import { Gitlab } from '@gitbeaker/rest';
import config from '../config.js';

export const GITLAB_DEVELOPER_ACCESS_LEVEL = 30;
export const GITLAB_MAINTAINER_ACCESS_LEVEL = 40;

export const api = new Gitlab({
	host: config.host,
	token: config.token,
});


export async function addOrUpdateCollaborator(project, collaborator, accessLevel, expiresAt) {
	if (!collaborator.id) {
		console.warn(`user id unknown for collaborator ${collaborator.email} - SKIPPED`);
		return;
	}

	const member = await api.ProjectMembers.all(project.id, {user_ids: [collaborator.id]});
	if (member.length === 1) {
		console.log(`updating ${collaborator.name} in ${project.name}`);
		await api.ProjectMembers.edit(project.id, collaborator.id, accessLevel, {expires_at: expiresAt});
	} else if (member.length === 0) {
		console.log(`adding ${collaborator.name} to ${project.name}`);
		await api.ProjectMembers.add(project.id, collaborator.id, accessLevel, {expires_at: expiresAt});
	} else {
		console.log(`unusual user count ${member.length}. Skipping`);
		console.log(member);
		return;
	}
}

export async function findUser(user) {
	console.log(`looking up user ${user.username} ...`);
	const foundUser = await api.Users.all({ search: user.name });

	if (foundUser.length === 1) {
		console.log(`... found (id ${foundUser[0].id} / username: ${foundUser[0].username})`);
		return foundUser[0];
	}

	console.warn(`${foundUser.length} users found for ${user.name}, this is currently not supported`);
}

export async function findProject(pathOrWebUrl) {
	let projectName = pathOrWebUrl;
	if (projectName.startsWith(config.host)) {
		projectName = projectName.substr(config.host.length + 1);
	}

	try {
		return await api.Projects.show(projectName, {simple: true});
	} catch (e) {
		throw `A project with the path or url ${pathOrWebUrl} could not be found: ${e}`;
	}
}

export async function findOrCreateFork(project, assignee) {
	let randomSuffix = '';
	if (config.randomSalt.length > 0) {
		randomSuffix = '-' + createHash('md5').update(config.randomSalt).update(project.name).update(assignee.name).digest('hex').substring(0,6);
	}

	const generatedForkInfo = {
		name: `${project.name} ${assignee.name}`,
		path: `${project.path}-${assignee.username}${randomSuffix}`,
		namespace_id: project.namespace.id // put it into the same namespace
	};

	try {
		const existingProject = await api.Projects.show(`${project.namespace.full_path}/${generatedForkInfo.path}`);
		return existingProject;
	} catch (error) {
		console.debug(`project ${generatedForkInfo.path} does not exist, yet. Now forking.`);
		return await api.Projects.fork(project.id, generatedForkInfo);
	}
}

export async function issuesFor(project) {
	return await api.Issues.all({projectId: project.id});
}
