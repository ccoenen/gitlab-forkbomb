import { stat } from 'fs/promises';

import chalk from 'chalk';
import groupBy from 'lodash/groupBy.js';
import simpleGit from 'simple-git';

import config from '../config.js';
import { forkIterator } from './fork-iterator.js';
import { issuesFor } from './api.js';


const projectURL = process.argv[2];
const action = process.argv[3];

forkIterator(projectURL, async (localPath, fork, upstream) => {
	//// good place to skip one in case of individual error
	// if (fork.name === 'something special') { continue; }
	console.log(`- processing ${fork.name} / ${fork.web_url}`);

	switch (action) {
	case 'status': {
		const git = simpleGit({baseDir: localPath});
		const result = await git.status();
		console.log(result.isClean() ? chalk.green("clean") : chalk.red("dirty"));
		break;
	}
	case 'pullOrClone': {
		try {
			await pullOrClone(localPath, fork.ssh_url_to_repo);
		} catch (e) {
			console.warn(`  ${e}`);
		}
		break;
	}
	case 'push': {
		const git = simpleGit({baseDir: localPath});
		const result = await git.push();
		// console.log(result); // internals usually not interesting
		console.log(`  pushed ${result.update?.hash?.from}...${result.update?.hash?.to}`);
		break;
	}
	case 'tag': {
		const tagName = process.argv[4];
	
		const git2 = simpleGit({baseDir: localPath});
		await git2.addTag(tagName).pushTags();
		break;
	}
	case 'distributeCommits': {
		const git2 = simpleGit({baseDir: localPath});
		await git2.fetch([upstream.ssh_url_to_repo]);
		try {
			const res = await git2.merge(['FETCH_HEAD']);
			console.log(`  ... ${res.result}: ${res.files.join(', ')}`);
		} catch (err) {
			console.error(`  merging failed for ${localPath}: ${err}!`);
		}
		break;
	}
	case 'resetToOriginHard': {
		const git = simpleGit({baseDir: localPath});
		git.reset('hard', ['origin']);
		break;
	}
	case 'commitsSince': {
		const timestamp = process.argv[4];
		const startDateTime = new Date(timestamp);
		const xOffset = 40;
		const now = new Date();
		const git2 = simpleGit({baseDir: localPath});
		let commits = await git2.log([`--since="${timestamp}"`, '--all']);
		let commitGroups = groupBy(commits.all, (c) => c.author_name);
		let issues = await issuesFor(fork);
		let issuesGroups = groupBy(issues, (c) => c.created_at.substring(0, 10));
		for (const author in commitGroups) {
			if (config?.report?.ignoreCommitsBy.includes(author)) continue;
			const commitsByDate = groupBy(commitGroups[author], (commit) => commit.date.substring(0, 10));
			const authorCommits = String(commitGroups[author].length);
			let runningClock = new Date(startDateTime.getTime());
			let graph = " ".repeat(xOffset - author.length - authorCommits.length);
			do {
				let lookupKey = runningClock.toISOString().substring(0, 10);
				let newSymbol = chalk.bgRed("?");
				if (!commitsByDate[lookupKey] || commitsByDate[lookupKey].length === 0) {
					if (runningClock.getDate() === 1) {
						newSymbol = chalk.gray("|");
					} else {
						newSymbol = chalk.gray("_");
					}
				} else if (commitsByDate[lookupKey].length === 1) {
					newSymbol = chalk.green(".");
				} else if (commitsByDate[lookupKey].length < 5) {
					newSymbol = chalk.green("i");
				} else if (commitsByDate[lookupKey].length < 10) {
					newSymbol = chalk.green("v");
				} else {
					newSymbol = chalk.green("X");
				}
				if (runningClock.getDay() === 0 || runningClock.getDay() === 6) {
					newSymbol = chalk.bgRgb(80, 0, 0)(newSymbol);
				}
				if (issuesGroups[lookupKey] && issuesGroups[lookupKey].length > 0) {
					newSymbol = chalk.bgRgb(20, 20, 140)(newSymbol);
				}

				graph += newSymbol;
				runningClock.setDate(runningClock.getDate() + 1);
			} while (runningClock.getTime() < now.getTime());
			console.log(`  - ${chalk.green(authorCommits)} commits from ${author} ${graph}`);
		}
		// Enable this if you want to link to the individual issues
		/*
		console.log(`  - ${issues.length} issues on this repo`);
		for (const issue of issues) {
			console.log(`    - #${issue.iid}: ${issue.title} (${issue.created_at}) ${issue.web_url}`)
		}
		*/
		break;
	}
	case 'commitsCount': {
		const git2 = simpleGit({baseDir: localPath});
		let commits = await git2.raw('rev-list', '--all', '--count');
		console.log(parseInt(commits, 10));
		break;
	}
	default: {
		console.error(`unknown action given: ${action}`);
		process.exit(1);
	}
	}
});

async function pullOrClone(localPath, ssh_url_to_repo) {
	const exists = await stat(localPath).catch(() => {});
	let action;
	try {
		if (!exists) {
			action = 'clone';
			await simpleGit().clone(ssh_url_to_repo, localPath);
		} else {
			action = 'pull';
			const pullResult = await simpleGit({ baseDir: localPath }).pull();
			let colorfulSummary = `${pullResult.files.length} files changed`;
			colorfulSummary = pullResult.files.length > 0 ? chalk.green(colorfulSummary) : chalk.red(colorfulSummary);
			console.log(`  ${colorfulSummary} (${pullResult.summary.changes + pullResult.summary.deletions + pullResult.summary.insertions} changed lines)`);
		}
	} catch (e) {
		throw `${action} failed: ${e}`;
	}
}
