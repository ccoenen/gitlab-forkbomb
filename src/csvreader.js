import { createReadStream } from 'fs';

import csv from 'fast-csv';

import { findUser } from './api.js';

function usernameFromEmail(email) {
	if (email) {
		const parts = email.split('@');
		return parts[0].toLowerCase();
	}
}

export async function read(filename) {
	return new Promise((resolve, reject) => {
		const fileStream = createReadStream(filename);
		const lines = [];
		const csvStream = csv.parse({headers: true, trim: true, comment: '#'})
			.on('data', (data) => {
				const preparedObject = {
					email: data.email.trim(),
					name: data.name.trim(),
					username: usernameFromEmail(data.email.trim()),
				};
		
				if (preparedObject.name.length < 1 || preparedObject.username.length < 1 || preparedObject.username.length < 1) {
					console.warn(`skipping entry in ${filename} because of missing info:\n  Row: ${JSON.stringify(data)}\n  Object: ${JSON.stringify(preparedObject)}`);
				} else {
					lines.push(preparedObject);
				}
			})
			.on('end', async () => {
				console.log(`CSV ${filename} has been parsed, found ${lines.length} entries`);
				for (const user of lines) {
					const gitlabUser = await findUser(user);
					if (gitlabUser?.id) {
						user.id = gitlabUser.id;
					}
				}
				resolve(lines);
			})
			.on('error', (err) => {
				return reject(`Failed to parse ${filename}: ${err.message}`);
			});

		fileStream.pipe(csvStream);
	});

}


