import chalk from 'chalk';
import groupBy from 'lodash/groupBy.js';
import maxBy from 'lodash/maxBy.js';
import simpleGit from 'simple-git';

import config from '../config.js';
import { forkIterator } from "./fork-iterator.js";
import { issuesFor } from './api.js';

const dataObject = [];
let earliestDate = (new Date()).getTime();
let latestDate = 0;

const projectURL = process.argv[2];
const outputType = process.argv[3] || 'text';

await forkIterator(projectURL, async (localPath, fork) => {
	const forkData = {
		localPath,
		fork,
		authors: [],
		issues: [],
		commitsByAuthor: {}
	};
	dataObject.push(forkData);

	const git = simpleGit({baseDir: localPath});
	let commits = await git.log(['--all']);
	commits.all.forEach((c) => {
		const commitTime = (new Date(c.date)).getTime();
		earliestDate = Math.min(earliestDate, commitTime);
		latestDate = Math.max(latestDate, commitTime);
	});

	forkData.issues = await issuesFor(fork);

	let commitGroups = groupBy(commits.all, (c) => c.author_name);
	for (const author in commitGroups) {
		if (config?.report?.ignoreCommitsBy.includes(author)) continue;
		forkData.authors.push(author);
		forkData.commitsByAuthor[author] = commitGroups[author];
	}
});


switch (outputType) {
	case 'text':
		renderTextReport(dataObject);
		break;
	case 'html':
		renderHTMLReport(dataObject);
		break;
	default:
		console.error("this can output text or html, please specify a parameter for that after the project url");
		process.exit(1);
}

function renderTextReport(projects) {
	const xOffset = 40;

	for (const project of projects) {
		console.log(`- Project: ${project.localPath}`);
	
		let issuesGroups = groupBy(project.issues, (c) => c.created_at.substring(0, 10));

		for (const author of project.authors) {
			const commitsByDate = groupBy(project.commitsByAuthor[author], (commit) => commit.date.substring(0, 10));
			let runningClock = new Date(earliestDate);
			const authorCommits = String(project.commitsByAuthor[author].length);
			let graph = " ".repeat(xOffset - author.length - authorCommits.length);
			do {
				let lookupKey = runningClock.toISOString().substring(0, 10);
				let newSymbol = chalk.bgRed("?");
				if (!commitsByDate[lookupKey] || commitsByDate[lookupKey].length === 0) {
					if (runningClock.getDate() === 1) {
						newSymbol = chalk.gray("|");
					} else {
						newSymbol = chalk.gray("_");
					}
				} else if (commitsByDate[lookupKey].length === 1) {
					newSymbol = chalk.green(".");
				} else if (commitsByDate[lookupKey].length < 5) {
					newSymbol = chalk.green("i");
				} else if (commitsByDate[lookupKey].length < 10) {
					newSymbol = chalk.green("v");
				} else {
					newSymbol = chalk.green("X");
				}
				if (runningClock.getDay() === 0 || runningClock.getDay() === 6) {
					newSymbol = chalk.bgRgb(80, 0, 0)(newSymbol);
				}
				if (issuesGroups[lookupKey] && issuesGroups[lookupKey].length > 0) {
					newSymbol = chalk.bgRgb(20, 20, 140)(newSymbol);
				}
			
				graph += newSymbol;
				runningClock.setDate(runningClock.getDate() + 1);
				
			} while (runningClock.getTime() < latestDate + 24 * 3600 * 1000);
			console.log(`  - ${authorCommits} commits by ${author} ${graph}`);
		}

		console.log(`  - ${project.issues.length} issues on this repo`);
		for (const issue of project.issues) {
			console.log(`    - #${issue.iid}: ${issue.title} (${issue.created_at}) ${issue.web_url}`)
		}
	}
}

function renderHTMLReport(projects) {
	const scaffold = (projectURL, content) => {
		return `<!DOCTYPE html>
		<html lang="de">
		  <head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>Report for ${projectURL}</title>
			<style>
				body {
					font-family: sans-serif;
				}
				details {
					border: 1px solid grey;
					border-radius: 9px;
					padding: 0.5em 1em;
				}
				details summary {
				}
				code {
					user-select: all;
				}
				table {
					border-collapse: collapse;
				}
				td[data-weekday="6"], td[data-weekday="0"] {
					background-color: rgb(255, 200, 200);
				}
				td[data-commit-count] {
					min-width: 20px;
					min-height: 20px;
					text-align: center;
				}
				td[data-commit-count="0"] {
					color: rgba(127, 127, 127, 0.5);
				}
				td[data-date$="-01"] {
					border-left: 2px solid grey;
				}
				td[data-has-unseen-commits="false"] {
					border-top: 2px solid blue;
				}
			</style>
		  </head>
		  <body>
				<main>
			${content}
				</main>
		  </body>
		  </html>`;
	};

	const content = projects.map((project) => {
		let unseenCommits = 0;

		const authorInfo = project.authors.map((author) => {
			let commitList = "";
			const commitsByDate = groupBy(project.commitsByAuthor[author], (commit) => commit.date.substring(0, 10));
			let runningClock = new Date(earliestDate);
			const lastIssue = maxBy(project.issues, (issue) => issue.created_at);
			const lastIssueAt = lastIssue ? new Date(lastIssue.created_at).getTime() : 0;

			do {
				let lookupKey = runningClock.toISOString().substring(0, 10);
				const commitCount = commitsByDate[lookupKey]?.length || 0;
				const hasUnseenCommits = lastIssueAt < runningClock.getTime();
				if (hasUnseenCommits) { unseenCommits += commitCount; }

				commitList += `<td data-date="${lookupKey}" data-commit-count="${commitCount}" title="${commitCount} commits on ${lookupKey}" data-weekday="${runningClock.getDay()}" data-has-unseen-commits="${hasUnseenCommits ? "true" : "false"}">${commitCount}</td>`;
				runningClock.setDate(runningClock.getDate() + 1);
			} while (runningClock.getTime() < latestDate + 24 * 3600 * 1000);

			return `<tr><td>${author}</td>${commitList}</tr>`;
		}).join("\n");

		let issueLinks = project.issues.map((i) => {
			return `<li><a href="${i.web_url}" target="feedback-window">#${i.iid} ${i.title} (${i.created_at})</a></li>`
		}).join("\n");
		issueLinks += `<li><a href="${project.fork.web_url}/-/issues/new?issue[title]=Feedback" target="feedback-window">NEW</a></li>`

		return `<details>
			<summary>${project.fork.name} <a href="${project.fork.web_url}" target="feedback-window">🌍</a> (<a href="${project.fork.web_url}/-/commits/main" target="feedback-window">${unseenCommits} ${unseenCommits > 0 ? "🟠" : "🔵"}</a>)</summary>
			
			<p>
				<code tabindex="0">cd "${project.localPath}"</code><br>
				<code tabindex="0">codium "${project.localPath}"</code><br>
				<code tabindex="0">nautilus "${project.localPath}" &</code>
			</p>
			<ul>${issueLinks}</ul>
			<table>
				<tr><th>Name</th></tr>
				${authorInfo}
			</table>
		</details>`;
	}).join("\n");

	console.log(scaffold(projectURL, content));
}