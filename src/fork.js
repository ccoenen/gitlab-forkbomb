import { read } from './csvreader.js';
import { addOrUpdateCollaborator, findOrCreateFork, findProject, GITLAB_MAINTAINER_ACCESS_LEVEL } from './api.js';

const projectURL = process.argv[2];
const assignees = await read(process.argv[3]);

const project = await findProject(projectURL);
const forks = [];

for (const assignee of assignees) {
	const forkedProject = await findOrCreateFork(project, assignee);
	const expiresAt = null; // TODO make this configurable.
	await addOrUpdateCollaborator(forkedProject, assignee, GITLAB_MAINTAINER_ACCESS_LEVEL, expiresAt);
	forks.push(forkedProject);
}

console.log('# created or updated');
forks.forEach(fork => {
	console.log(`- ${fork.web_url} (id ${fork.id})`);
});
