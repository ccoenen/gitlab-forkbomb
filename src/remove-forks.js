import { api, findProject } from './api.js';

const projectURL = process.argv[2];
const actuallyDeleteThemAll = process.argv[3] === '--actually-delete-them-all';

const project = await findProject(projectURL);
const forks = await api.Projects.forks(project.id);

console.log(`found ${forks.length} forks of ${project.name}`);
for (const fork of forks) {
	console.log(`- ${fork.name} (id ${fork.id}) at ${fork.web_url}`);
}

if (!actuallyDeleteThemAll) {
	console.log('delete flag was not supplied. Exiting. Use `--actually-delete-them-all` to nuke all of the above.');
	process.exit();
}

for (const fork of forks) {
	console.log(`- attempting to remove fork ${fork.name} (id ${fork.id}) ...`);
	const result = await api.Projects.remove(fork.id);
	if (result && result.message === '202 Accepted') {
		console.log('  ... OK');
	} else {
		console.log(`  something went wrong: ${result}`);
	}
}
