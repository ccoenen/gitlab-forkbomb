import { readdir } from 'node:fs/promises';

import simpleGit from 'simple-git';
import { format } from '@fast-csv/format';

import config from '../config.js';
import { forkIterator } from './fork-iterator.js';

const projectURL = process.argv[2];

const subpaths = [
];

const ignorePaths = [
	'README.md',
	'.git',
	'.DS_Store',
	'.idea',
];

const csvStream = format({headers: ['name', ...subpaths, 'remainingPaths']});
csvStream.pipe(process.stdout);

await forkIterator(projectURL, async (localPath, fork) => {
	const row = {name: fork.name};
	const git = simpleGit({baseDir: localPath});
	const remainingPaths = (await readdir(localPath))
		.filter((path) => !ignorePaths.includes(path))
		.filter((path) => !subpaths.includes(path));
	for (const subpath of subpaths) {
		const result = await git.log({file: subpath});
		const filtered = result.all.filter(commit => !config?.report?.ignoreCommitsBy.includes(commit.author_name));
		row[subpath] = filtered.length;
	}
	row.remainingPaths = remainingPaths;

	csvStream.write(row);
});
