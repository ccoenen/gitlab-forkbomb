import { api } from './api.js';

const projects = await api.Projects.all({simple: true, visibility: 'internal'});
for (const project of projects) {
	console.log(`${project.name} in ${project.path_with_namespace} (id: ${project.id}) ${project.web_url}`);
	api.Projects.edit(project.id, {visibility: 'private'});
}
