import { mkdir } from 'fs/promises';
import { resolve } from 'path';

import { api, findProject } from './api.js';

/**
 * iterates over all forks of a project
 * @param {String} projectURL 
 * @param {function(path, Project)} callback 
 */
export async function forkIterator(projectURL, callback) {
    const upstream = await findProject(projectURL);
    const forks = await api.Projects.allForks(upstream.id);
    forks.sort((a, b) => a.path.localeCompare(b.path)); // stable, deterministic sorting of the result
    forks.push(Object.assign({}, upstream, {name: `${upstream.name} upstream`, _overriddenLocalPath: `_upstream ${upstream.path}`}));

    const basePath = resolve(upstream.namespace.path, upstream.path);
    await mkdir(basePath, { recursive: true });

    for (const fork of forks) {
        const localPath = resolve(basePath, fork._overriddenLocalPath || fork.path.substr(upstream.path.length + 1));
        await callback(localPath, fork, upstream);
    }
}
